# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 11:59:33 2017

@author: Darius
"""

import matplotlib
import datetime
import matplotlib.pyplot as plt
import numpy as np
import random

def polyfit(dates, levels, p):
    """calculates the least squares fit to degree p of the 
    data for level and dates"""

    d = matplotlib.dates.date2num(dates) 
    # measures the number of days since the year 0000 until now
    
    #startdate = currentdate - dates # the day from which the analysis will start
    #d = dates
    # Create set of 10 data points on interval (startdate, currentdate)
   # print (dates)
    # Using shifted d values, find coefficient of best-fit
    # polynomial f(x) of degree 4
    p_coeff = np.polyfit((d - d[0]), levels, p)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    # Plot original data points
    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    d1 = np.linspace(d[0], d[-1], 30)
    #return(plt.plot(d1, poly(d1 - d[0])), plt.plot(d, levels))
    return poly