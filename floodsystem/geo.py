#from .utils import sorted_by_key
from floodsystem.stationdata import build_station_list
from collections import OrderedDict
from operator import itemgetter
from haversine import haversine
# imports the haversine formula for use in the program

stations = build_station_list()
N=9
#1st part
def rivers_with_station(stations):
    rivers=[]
    a=0
    
    for station in stations:
        if station.river in rivers:
            a=a+1
        else:
            rivers.append(station.river)
    return rivers
    
#2nd part
#array is created for each river and is filled with the station name, to prevent creating several arrays for each river....
def stations_by_river(stations):
    dic={}
    rivers=[]
    for station in stations:
        if station.river in rivers:
            dic[station.river].append(station.name)
            dic[station.river].sort()
        else:
            rivers.append(station.river)
            dic[station.river]=[]
            dic[station.river].append(station.name)
            dic[station.river].sort()
    return(dic)
    
def rivers_by_station_number(stations, N):
    dic2={}
    dic={}
    new_list=[]
    rivers=[]
    for station in stations:
        if station.river in rivers:
            dic[station.river].append(station.name)
        else:
            rivers.append(station.river)
            dic[station.river]=[]
            dic[station.river].append(station.name)
        dic2[station.river]=len(dic[station.river])
        sorted_dic = OrderedDict(sorted(dic2.items(), key=itemgetter(1),reverse=True))
    for x,y in sorted_dic.items():
        new_list.append((x,y))
    return new_list[:N]
            
"""function that takes a set of stations and implements the haversine 
formula on thier coordinates and then sorts them by distance"""
def stations_by_distance(stations, p):
# function as requested
    stations = [ (station.name, station.coord) for station in stations]
# makes a new dictionary with only the name and coordinates of the station
    for i in range(len(stations)): stations[i] = stations[i] + (haversine(p,stations[i][1]),)
# 
    stations = sorted(stations , key = lambda station: station[2]) 
    stations = [ ( i[0],i[2] ) for i in stations ]
# sorts the stations by distance from p, by using the 3 column  
    return (stations [x:y])

def stations_within_radius(stations, centre, r):
#function name as defined    
    stations = [ (station.name, station.coord) for station in stations]
    # makes a new dictionary with only the name and coordinates of the station
    stations = [ e for e in stations if haversine(centre,e[1]) < r ]
    # only adds stations to the new dictionary if the distance from the centre
    # as calculated by the haversine formula is less than the radius r
    print( sorted([ e[0] for e in stations ]))
   