# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 22:28:19 2017

@author: Alexandre
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    x=[]
    for station in stations:
        y=station.relative_water_level()
        if y==None:
            pass;
        elif y>tol:
            x.append((station.name,y))
    y=sorted( x , key = lambda e : e[1], reverse=True )
            
    return y
    
"""def stations_highest_rel_level(stations, N):
    x=[]
    update_water_levels(stations)
    for station in stations:
        y=station.relative_water_level()
        if y==None:
            pass;
        elif y>station.typical_range[1]:
            x.append((station.name,y))
    y=sorted( x , key = lambda e : e[1], reverse=True )
            
    return y[:N]"""
        
def stations_highest_rel_level(stations, N):
    x=[]
    for station in stations:
        if station.relative_water_level() == None:
            pass
        else:
            x.append((station.name, station.relative_water_level()))
    x = sorted_by_key(x, 1, reverse = True)
            
    return x[:N]           