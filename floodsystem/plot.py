# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 23:54:22 2017

@author: Alexandre
"""
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.datafetcher import fetch_latest_water_level_data
from floodsystem.analysis import polyfit
from floodsystem import analysis
import json
import requests
import sys
import random
import matplotlib
import matplotlib.pyplot as plt
import numpy as np




#dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))    

def plot_water_levels(station, dates, levels):
    
    levels1= [station.typical_range[0]]*len(levels)
    levels2=[station.typical_range[1]]*len(levels)

    plt.plot(dates, levels,label="actual water level")
    plt.plot(dates, levels1,label="typical low water level")
    plt.plot(dates, levels2,label="typical high water level")

# Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)

# Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
    return plt.show()
    
def plot_water_level_with_fit(station, dates, levels, p):
    #print(np.array(polyfit(dates,levels,p)).shape())
    #plt.plot(polyfit(dates, levels, p))
    plt.plot(dates,levels)
    dates = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(dates - dates[0], levels, 4)

    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)
    x1 = np.linspace(dates[0], dates[-1], 30)
    plt.plot(x1, poly(x1 - dates[0]))
    
    # Display plot
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
# Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
    return plt.show()