# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 11:38:47 2017

@author: Darius
"""

stations = build_station_list()

centre = (52.2053, 0.1218)

r = 10

def run():
    D = stations_within_radius(stations, centre, r)
       
    print (D)
    
run()