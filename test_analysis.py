# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 11:33:58 2017

@author: Alexandre
"""

import pytest
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import polyfit
import datetime


def test_polyfit():
    """Test building list of stations"""
    p=4        
    dates=[datetime.datetime(2017, 2, 23, 13, 15,)]
    levels=[2.0]

    station_list = polyfit(dates, levels, p)
    assert len(station_list) > 0

