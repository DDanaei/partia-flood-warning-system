# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 21:28:14 2017

@author: Alexandre
"""
from floodsystem.stationdata import build_station_list

stations = build_station_list()

#functions are defined in geo.py
def run():
    N=rivers_with_station(stations)
    N.sort()
    print("Number of rivers:{}".format(len(N)))
    print(N[:10])
    
    

    Y=stations_by_river(stations)
    for riv,name in Y.items():
        if riv in ['River Aire', 'River Cam', 'Thames']:
            print(riv,name)

run()