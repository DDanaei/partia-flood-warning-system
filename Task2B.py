# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 23:01:39 2017

@author: Alexandre
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold


def run():
    # Build list of stations
    stations = build_station_list()
    tol=0.8

    # Update latest level data for all stations
    update_water_levels(stations)

    y = stations_level_over_threshold(stations,tol)

    for i in y:
        print (i[0], i[1])
run()

