# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 11:27:20 2017

@author: Alexandre
"""

import pytest
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold,stations_highest_rel_level
from floodsystem.station import MonitoringStation



def test_stations_level_over_threshold():
    """Test building list of stations"""
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s.relative_water_level=lambda:0.05
    tol = 0.8
    
    station_list = stations_level_over_threshold([s], tol)
    assert len(station_list)== 0


def test_stations_highest_rel_level():
    """Test update to latest water level"""

     # Build list of stations
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    s.relative_water_level=lambda:0.05
    
    N=1
    
    station_list = stations_highest_rel_level([s], N)
    assert len(station_list) > 0

    