# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 22:34:43 2017

@author: Alexandre
"""
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime

stations = build_station_list()
tol=0
x=[]

    # Update latest level data for all stations
update_water_levels(stations)

for station in stations:
    y=station.relative_water_level()
    if y==None:
        pass;
    elif y<1.0:
        state="Normal"
    elif y<1.4:
        state="Low"
    elif y<1.7:
        state="Moderate"
    elif y<2.1:
        state="High"
    else:
        state="Severe"
    x.append((station.name,state,station.town))
List_of_station=sorted( x , key = lambda e : e[1], reverse=True )
#print(List_of_station)
print("Low")
print(" ")
for name in List_of_station:
    if name[1]=="Low":
        print(name[2])
print(" ")        
print("Moderate")
print(" ")
for name in List_of_station:
    if name[1]=="Moderate":
        print(name[2])
print(" ")
print("High")
print(" ")
for name in List_of_station:
    if name[1]=="High":
        print(name[2])
print(" ")
print("Severe")
print(" ")
for name in List_of_station:
    if name[1]=="Severe":
        print(name[2])

x=0
for name in List_of_station:
    if name[1]=="Severe":
        print(name[2])
        x=x+1
        
        # Build list of stations
        stations = build_station_list()
        N = x
        # Update latest level data for all stations
        update_water_levels(stations)
    
    
        #obtaining the stations with N highest relative water level
        xyz=stations_highest_rel_level(stations, N)
    
        a, b, = zip(*xyz)
    
        for station in stations:
        
            if station.name in a:
                dt = 5  
                p = 4
                dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))  
                #print (dates, levels)
                print (plot_water_level_with_fit(station, dates, levels, p))
            else:
                pass
             
if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")        
        


        