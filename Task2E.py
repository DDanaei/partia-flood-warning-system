# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 00:12:35 2017

@author: Alexandre
"""
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels
import datetime

#%matplotlib inline

def run():
    # Build list of stations
    stations = build_station_list()
    N=5
    
    # Update latest level data for all stations
    update_water_levels(stations)
    
    
    #obtaining the stations with 5 highest relative water level
    xyz=stations_highest_rel_level(stations, N)
    
    a, b, = zip(*xyz)
    print (a)
    #for names in xyz:
     #   name_of_stations.append(names[0])
    
    #for station in name_of_stations:
     #   dt = 10   
      #  dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))  
       # plot_water_levels(station, dates, levels)
    
    for station in stations:
        
        if station.name in a:
            dt = 10   
            dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))  
            print (plot_water_levels(station, dates, levels))
        else:
            pass
             
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")        
        
run()