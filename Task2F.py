# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 23:20:32 2017

@author: Darius
"""

from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
import datetime


def run():
    # Build list of stations
    stations = build_station_list()
    N=5
    
    # Update latest level data for all stations
    update_water_levels(stations)
    
    
    #obtaining the stations with N highest relative water level
    xyz=stations_highest_rel_level(stations, N)
    
    a, b, = zip(*xyz)
    print (a)
    
    for station in stations:
        
        if station.name in a:
            dt = 5  
            p = 4
            dates, levels = fetch_measure_levels(station.measure_id,dt=datetime.timedelta(days=dt))
            print(dates)
            #print (dates, levels)
            print (plot_water_level_with_fit(station, dates, levels, p))
        else:
            pass
             
if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")        
        
run()