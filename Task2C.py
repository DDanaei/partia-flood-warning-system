# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 23:29:55 2017

@author: Alexandre
"""

from floodsystem.stationdata import build_station_list, update_water_levels


def run():
    # Build list of stations
    stations = build_station_list()
    N=10

    # Update latest level data for all stations
    update_water_levels(stations)
    
    y=(stations_highest_rel_level(stations, N))
    
    for i in y:
        print (i[0], i[1])
    

run()